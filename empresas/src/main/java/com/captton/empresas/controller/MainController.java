package com.captton.empresas.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.empresas.model.Banco;
import com.captton.empresas.model.Empleado;
import com.captton.empresas.model.Empresa;
import com.captton.empresas.model.InputEmpleado;
import com.captton.empresas.model.InputEmpresa;
import com.captton.empresas.repository.BancoDao;
import com.captton.empresas.repository.EmpleadoDao;
import com.captton.empresas.repository.EmpresaDao;

@RestController
@RequestMapping({"/registro"})
public class MainController {
	
	@Autowired
	private BancoDao bancodao;
	
	@Autowired
	private EmpresaDao empresadao;
	
	@Autowired
	private EmpleadoDao empleadodao;
	
	//doy de alta un banco
	@PostMapping
	public Banco createBanco(@RequestBody Banco banco) {
		
		return bancodao.save(banco);
	}
	
	//doy de alta una empresa, asignandole un banco desde el frontend
	@PostMapping(path= {"/empresas"})
	public ResponseEntity<Object> createEmpresa(@RequestBody InputEmpresa empresa) {
		
		Banco banco1 = bancodao.findById(empresa.getIdBanco()).orElse(null);
		Empresa emp1 = new Empresa();
		
		JSONObject obj = new JSONObject();
		
		if(banco1 != null) {
			
			emp1.setBanco(banco1);
			emp1.setNombre(empresa.getEmpresa().getNombre());
			emp1.setDireccion(empresa.getEmpresa().getDescripcion());
			emp1.setLocalidad(empresa.getEmpresa().getLocalidad());
			emp1.setDescripcion(empresa.getEmpresa().getDescripcion());
			
			Empresa empre = empresadao.save(emp1);
			
			obj.put("error", 0);
			obj.put("message", empre.getId());
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			obj.put("error", 1);
			obj.put("message", "BANCO NOT FOUND");
			
			return ResponseEntity.ok().body(obj.toString());
		}
	}
	
	//doy de alta un empleado, asignandole una empresa desde el frontend (muy similar al API anterior)
	@PostMapping(path= {"/empleados"})
	public ResponseEntity<Object> createEmpleado(@RequestBody InputEmpleado empleado) {
		
		Empresa empresa1 = empresadao.findById(empleado.getIdEmpresa()).orElse(null);
		Empleado empleado1 = new Empleado();
		
		JSONObject obj = new JSONObject();
		
		if(empresa1 != null) {
			
			empleado1.setEmpresa(empresa1);
			empleado1.setNombre(empleado.getEmpleado().getNombre());
			empleado1.setApellido(empleado.getEmpleado().getApellido());
			empleado1.setDireccion(empleado.getEmpleado().getDireccion());
			empleado1.setDni(empleado.getEmpleado().getDni());
			
			Empleado empleadou = empleadodao.save(empleado1);
			
			obj.put("error", 0);
			obj.put("message", empleadou.getId());
			
			return ResponseEntity.ok().body(obj.toString());
	
		} else {
			
			obj.put("error", 1);
			obj.put("message", "EMPRESA NOT FOUND");
			
			return ResponseEntity.ok().body(obj.toString());
			
			
		}
	}
	
	@PutMapping(value ="/empleados/{id}")
	public ResponseEntity<Object> modificarDireccion(@PathVariable("id") Long id, @RequestBody Empleado emple) {
		
		Empleado emp1;
		
		JSONObject obj = new JSONObject();
		
		emp1 = empleadodao.findById(id).orElse(null);
		
		if(emp1 != null) {
			
			emp1.setDireccion(emple.getDireccion());
			
			empleadodao.save(emp1);
			
			obj.put("error", 0);
			obj.put("message", emp1.getId());
			
		} else {
			
			obj.put("error", 1);
			obj.put("message", "NO SE ENCONTRO EMPLEADO");
			
		}
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	@GetMapping(path= {"/empresas/{id}"})
	public ResponseEntity<Object> listarEmpresas(@PathVariable("id") Long id) {
		
		Empresa empre1 = empresadao.findById(id).orElse(null);
		
		if(empre1 != null) {
		
		List<Empleado> empleadosFound = empleadodao.findAll();
		
		JSONArray json_array = new JSONArray();
		
		if(empleadosFound.size() > 0) {
			
			for(Empleado emp : empleadosFound) {
				
				JSONObject aux = new JSONObject();
				aux.put("nombre", emp.getNombre());
				aux.put("apellido", emp.getApellido());
				aux.put("dni", emp.getDni());
				aux.put("direccion", emp.getDireccion());
				
				json_array.put(aux); // meto el objeto json con los datos del empleado al array de empleados
				
			}
			
		} else {
			
			JSONObject objerror = new JSONObject();
			objerror.put("error", 1);
			objerror.put("results", "NO HAY EMPLEADOS");
			
			return ResponseEntity.ok().body(objerror.toString());
			
			
		}
		
		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		obj.put("nombreempresa", empre1.getNombre());
		obj.put("direccionempresa", empre1.getDireccion());
		obj.put("listaempleados:", json_array);
		
		return ResponseEntity.ok().body(obj.toString());
		
		} else {
			
			JSONObject objerror = new JSONObject();
			objerror.put("error", 1);
			objerror.put("results", "EMPRESA NOT FOUND");
			
			return ResponseEntity.ok().body(objerror.toString());
			
		}
		
	}
	
	
	
	

}
