package com.captton.empresas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.empresas.model.Empresa;

public interface EmpresaDao extends JpaRepository<Empresa, Long> {

}
