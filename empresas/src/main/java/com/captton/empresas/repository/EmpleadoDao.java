package com.captton.empresas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.empresas.model.Empleado;

public interface EmpleadoDao extends JpaRepository<Empleado, Long> {

}
