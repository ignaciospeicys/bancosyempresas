package com.captton.empresas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.empresas.model.Banco;

public interface BancoDao extends JpaRepository<Banco, Long> {

}
